#!/bin/sh

set -e

if [ -z "$GITLAB_CI_SSH_KEY" ]; then
	echo "Missing environment variable for GITLAB_CI_SSH_KEY"
	exit 1
fi

gitlab-runner exec docker --env SSH_PRIVATE_KEY="`cat $GITLAB_CI_SSH_KEY`" $*
